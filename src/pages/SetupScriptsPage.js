import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBEdgeHeader,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink,
  MDBContainer,
  MDBIcon,
  MDBBtn
} from 'mdbreact';
import DocsLink from '../components/docsLink';

import './HomePage.css';
class SetupScriptsPage extends React.Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false
  };

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    const { sideNavRight, sideNavLeft } = this.state;

    return (
      <Router>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '160px' }} />
        <div className="d-flex justify-content-between">
          <div className="p-2 text-left">
            <MDBBtn onClick={this.sidenavToggle('Left')}>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              slim
              mask='rgba-blue-strong'
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='sn-bg-1'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel target="_parent">
                  <MDBIcon icon='pencil-alt' className='mr-2' />
                  Home
                </MDBSideNavLink>
                <MDBSideNavLink to='/repository' topLevel>
                  <MDBIcon icon='pencil-alt' className='mr-2' />
                  Repository (git)
                </MDBSideNavLink>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink>Clone Repository</MDBSideNavLink>
                  <MDBSideNavLink>Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink>Dependencies</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <MDBSideNavLink>Create React App</MDBSideNavLink>
                  <MDBSideNavLink>React Router</MDBSideNavLink>
                  <MDBSideNavLink>Service Worker</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                  <MDBSideNavLink>MDBootstrap</MDBSideNavLink>
                  <MDBSideNavLink>Color Management</MDBSideNavLink>
                  <MDBSideNavLink>Font Management</MDBSideNavLink>
                  <MDBSideNavLink>Templates</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink>FAQ</MDBSideNavLink>
                  <MDBSideNavLink>Write a message</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left">
            <MDBBtn onClick={this.sidenavToggle('Right')} className='rws'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
              slim
              triggerOpening={sideNavRight}
              className='side-nav-light'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://mdbootstrap.com/img/Photos/Avatars/avatar-1-mini.jpg'
                      className='rounded-circle'
                    />
                    <span className='text-black-50'>John Smith</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
                <MDBSideNavCat
                  name='Submit blog'
                  id='submit-blog2'
                  icon='chevron-right'
                >
                  <MDBSideNavLink className='active'>
                    Submit listing
                  </MDBSideNavLink>
                  <MDBSideNavLink>Registration form</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Instruction'
                  id='instruction2'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>For bloggers</MDBSideNavLink>
                  <MDBSideNavLink>For authors</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat name='About' id='about2' icon='eye'>
                  <MDBSideNavLink>Instruction</MDBSideNavLink>
                  <MDBSideNavLink>Monthly meetings</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me2'
                  icon='envelope'
                >
                  <MDBSideNavLink>FAQ</MDBSideNavLink>
                  <MDBSideNavLink>Write a message</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>
        <MDBContainer>
              <DocsLink
                title='Setup | Scripts'
                href='https://mdbootstrap.com/docs/react/navigation/sidenav/'
              />
              <h4 className="mb2">Scripts</h4>
            </MDBContainer>
        
      </Router>
    );
  }
}

export default SetupScriptsPage;
