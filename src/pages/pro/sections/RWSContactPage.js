import React from 'react';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBBtn,
  MDBInput
} from 'mdbreact';
import DocsLink from '../../../components/docsLink';
import SectionContainer from '../../../components/sectionContainer';

const RWSContactPage = () => {
  return (
    <MDBContainer>
      <DocsLink
        title='Contact Sections'
        href='https://mdbootstrap.com/docs/react/sections/contact/'
      />

      <SectionContainer tag='section'>
        <h2 className='h1-responsive font-weight-bold text-center my-5'>
          Contact Me
        </h2>
        <p className='text-center w-responsive mx-auto pb-5'>
          Hier haben Sie die Möglichkeit mit mir über meine verschiedenen Kanäle in Kontakt zu treten. Ich melde mich bei Ihnen zeitnah, meist innerhalb der nächsten 24 Stunden.
        </p>
        <MDBRow>
          <MDBCol lg='5' className='lg-0 mb-4'>
            <MDBCard>
              <MDBCardBody>
                <div className='form-header blue accent-1'>
                  <h3 className='mt-2'>
                    <MDBIcon icon='envelope' /> Schreiben Sie mir:
                  </h3>
                </div>
                <p className='dark-grey-text'>
                  Je detaillierter Sie Ihre Anfrage stellen, desto besser kann ich auf Ihre Anfrage eingehen.
                </p>
                <div className='md-form'>
                  <MDBInput
                    icon='user'
                    label='Ihr Name'
                    iconClass='grey-text'
                    type='text'
                    id='form-name'
                  />
                </div>
                <div className='md-form'>
                  <MDBInput
                    icon='envelope'
                    label='Ihre E-Mail'
                    iconClass='grey-text'
                    type='text'
                    id='form-email'
                  />
                </div>
                <div className='md-form'>
                  <MDBInput
                    icon='tag'
                    label='Betreff'
                    iconClass='grey-text'
                    type='text'
                    id='form-subject'
                  />
                </div>
                <div className='md-form'>
                  <MDBInput
                    icon='pencil-alt'
                    label='Nachricht'
                    iconClass='grey-text'
                    type='textarea'
                    id='form-text'
                  />
                </div>
                <div className='text-center'>
                  <MDBBtn color='light-blue'>Abschicken</MDBBtn>
                </div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol lg='7'>
            <div
              id='map-container'
              className='rounded z-depth-1-half map-container'
              style={{ height: '400px' }}
            >
              <iframe
                src='https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d10494.9521496421!2d8.99826075!3d48.88227025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sde!2sde!4v1584639785677!5m2!1sde!2sde'
                title='This is a unique title'
                width='100%'
                height='100%'
                frameBorder='0'
                style={{ border: 0 }}
              />
            </div>
            <br />
            <MDBRow className='text-center'>
              <MDBCol md='4'>
                <MDBBtn tag='a' floating color='blue' className='accent-1'>
                  <MDBIcon icon='map-marker-alt' />
                </MDBBtn>
                <p>71735 Eberdingen</p>
                <p className='mb-md-0'>Baden Württemberg</p>
              </MDBCol>
              <MDBCol md='4'>
                <MDBBtn tag='a' floating color='blue' className='accent-1'>
                  <MDBIcon icon='phone' />
                </MDBBtn>
                <p>+ 49 173 72 100 87</p>
                <p className='mb-md-0'>Mo - Fri, 8:00-22:00</p>
              </MDBCol>
              <MDBCol md='4'>
                <MDBBtn tag='a' floating color='blue' className='accent-1'>
                  <MDBIcon icon='envelope' />
                </MDBBtn>
                <p>hauke.juergen@gmail.com</p>
                <p className='mb-md-0'>admin@web-complett.de</p>
              </MDBCol>
            </MDBRow>
          </MDBCol>
        </MDBRow>
      </SectionContainer>
    </MDBContainer>
  );
};

export default RWSContactPage;
