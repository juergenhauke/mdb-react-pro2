import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  MDBEdgeHeader,
  MDBSideNavCat,
  MDBSideNavNav,
  MDBSideNav,
  MDBSideNavLink,
  MDBFreeBird,
  MDBCardBody,
  MDBCard,
  MDBCardTitle,
  MDBCardImage,
  MDBCardText,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBAnimation,
  MDBIcon,
  MDBBtn,
  MDBNavLink
} from 'mdbreact';
import DocsLink from '../components/docsLink';
import SectionContainer from '../components/sectionContainer';
import './HomePage.css';
class HomePage extends React.Component {
  state = {
    sideNavLeft: false,
    sideNavRight: false
  };

  sidenavToggle = sidenavId => () => {
    const sidenavNr = `sideNav${sidenavId}`;
    this.setState({
      [sidenavNr]: !this.state[sidenavNr]
    });
  };

  scrollToTop = () => window.scrollTo(0, 0);
  render() {
    const { sideNavRight, sideNavLeft } = this.state;

    return (
      <Router>
        <MDBEdgeHeader color='indigo darken-3' className='sectionPage align-items-stretch' style={{ height: '160px' }} />
        <div className="d-flex justify-content-between">
          <div className="p-2 text-left">
            <MDBBtn onClick={this.sidenavToggle('Left')}>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the left SideNav: */}
            <MDBSideNav
              slim
              mask='rgba-blue-strong'
              triggerOpening={sideNavLeft}
              breakWidth={1300}
              className='sn-bg-1'
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg'
                      className='rounded-circle'
                    />
                    <span>Documentations</span>
                  </a>
                </div>
              </li>

              <MDBSideNavNav>
                <MDBSideNavLink to='/' topLevel>
                  <MDBIcon icon='home' className='mr-2' />
                  Home
                </MDBSideNavLink>
                <MDBSideNavLink to='/other-page' topLevel>
                  <MDBIcon icon='pencil-alt' className='mr-2' />
                  Repository (git)
                </MDBSideNavLink>
                <MDBSideNavCat
                  name='Setup'
                  id='setup'
                  icon='chevron-right'
                >
                  <MDBSideNavLink to='/project-setup' target="_parent">Project Setup</MDBSideNavLink>
                  <MDBSideNavLink to='/scripts' target="_parent">Start / Build / Test</MDBSideNavLink>
                  <MDBSideNavLink to='/dependencies' target="_parent">Dependencies</MDBSideNavLink>
                  <MDBSideNavLink to='/webpack' target="_parent">Webpack</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='React'
                  id='react'
                  icon='hand-pointer'
                  href='#'
                >
                  <MDBSideNavLink to='/create-react-app' target="_parent">Create React App</MDBSideNavLink>
                  <MDBSideNavLink to='/react-router' target="_parent">React Router</MDBSideNavLink>
                  <MDBSideNavLink to='/service-worker' target="_parent">Service Worker</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat name='Design' id='about' icon='eye'>
                  <MDBSideNavLink to='/mdbootstrap' target="_parent">MDBootstrap</MDBSideNavLink>
                  <MDBSideNavLink to='/color-management' target='_parent'>Color Management</MDBSideNavLink>
                  <MDBSideNavLink to='/font-management' target='_parent'>Font Management</MDBSideNavLink>
                  <MDBSideNavLink to='/templates' target='_parent'>Templates</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me'
                  icon='envelope'
                >
                  <MDBSideNavLink to='/sections/rws-contact' target="_parent">Contact</MDBSideNavLink>
                  <MDBSideNavLink>Write a message</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
          <div className="p-2 text-left">Flex item 2</div>
          <div className="p-2 text-left">
            <MDBBtn onClick={this.sidenavToggle('Right')} className='rws'>
              <MDBIcon size='lg' icon='bars' />
            </MDBBtn>
            {/* the right SideNav: */}
            <MDBSideNav
              slim
              triggerOpening={sideNavRight}
              className='side-nav-light'
              right
              breakWidth={1300}
            >
              <li>
                <div className='logo-wrapper sn-ad-avatar-wrapper'>
                  <a href='#!'>
                    <img
                      alt=''
                      src='https://mdbootstrap.com/img/Photos/Avatars/avatar-1-mini.jpg'
                      className='rounded-circle'
                    />
                    <span className='text-black-50'>John Smith</span>
                  </a>
                </div>
              </li>
              <li>
                <ul className='social'>
                  <li>
                    <MDBIcon brand icon='facebook' />
                  </li>
                  <li>
                    <MDBIcon brand icon='pinterest' />
                  </li>
                  <li>
                    <MDBIcon brand icon='google-plus' />
                  </li>
                  <li>
                    <MDBIcon brand icon='twitter' />
                  </li>
                </ul>
              </li>
              <MDBSideNavNav>
                <MDBSideNavCat
                  name='Submit blog'
                  id='submit-blog2'
                  icon='chevron-right'
                >
                  <MDBSideNavLink className='active'>
                    Submit listing
                  </MDBSideNavLink>
                  <MDBSideNavLink>Registration form</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Instruction'
                  id='instruction2'
                  icon='hand-pointer'
                >
                  <MDBSideNavLink>For bloggers</MDBSideNavLink>
                  <MDBSideNavLink>For authors</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat name='About' id='about2' icon='eye'>
                  <MDBSideNavLink>Instruction</MDBSideNavLink>
                  <MDBSideNavLink>Monthly meetings</MDBSideNavLink>
                </MDBSideNavCat>
                <MDBSideNavCat
                  name='Contact me'
                  id='contact-me2'
                  icon='envelope'
                >
                  <MDBSideNavLink>FAQ</MDBSideNavLink>
                  <MDBSideNavLink>Write a message</MDBSideNavLink>
                </MDBSideNavCat>
              </MDBSideNavNav>
            </MDBSideNav>
          </div>
        </div>
        <SectionContainer header='Slim' flexCenter>
          <div className='mt-3 mb-5'>
            <MDBFreeBird>
              <MDBRow>
                <MDBCol
                  md='10'
                  className='mx-auto float-none white z-depth-1 py-2 px-2'
                >
                  <MDBCardBody className='text-center'>
                    <h2 className='h2-responsive mb-4'>
                      <strong className='font-weight-bold'>
                        <img
                          src='https://web-complett.de/mdb-react-pro/logo/logo-transparent-250.png'
                          alt='mdbreact-logo'
                          className='pr-2'
                        />
                      </strong>
                    </h2>
                    <MDBRow />
                    <p>React Bootstrap with Material Design</p>
                    <p className='pb-4'>
                      This application shows the actual use of MDB React
                      components in the application.
                  </p>
                    <MDBRow className='d-flex flex-row justify-content-center row'>
                      <a
                        className='border nav-link border-light rounded mr-1 mx-2 mb-2'
                        href='https://mdbootstrap.com/react/'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        <MDBIcon icon='graduation-cap' className='mr-2' />
                        <span className='font-weight-bold'>
                          Official Documentation
                      </span>
                      </a>
                      <a
                        className='border nav-link border-light rounded mx-2 mb-2'
                        href='https://mdbootstrap.com/products/react-ui-kit/'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        <MDBIcon far icon='gem' className='mr-2' />
                        <span className='font-weight-bold'>PRO</span>
                      </a>
                      <a
                        className='border nav-link border-light rounded mx-2 mb-2'
                        href='https://mdbootstrap.com/docs/react/getting-started/download/'
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        <MDBIcon icon='download' className='mr-2' />
                        <span className='font-weight-bold'>FREE</span>
                      </a>
                    </MDBRow>
                  </MDBCardBody>
                </MDBCol>
              </MDBRow>
            </MDBFreeBird>

            <MDBContainer>
              <MDBRow>
                <MDBCol md='12' className='mt-4'>
                  <h2 className='text-center my-5 font-weight-bold'>
                    Why is it so great?
                </h2>
                  <p className='text-center text-muted mb-1'>
                    Google has designed a Material Design to make the web more
                    beautiful and more user-friendly.
                </p>
                  <p className='text-center text-muted mb-1'>
                    Twitter has created a Bootstrap to support you in faster and
                    easier development of responsive and effective websites.
                </p>
                  <p className='text-center text-muted'>
                    We present you a framework containing the best features of
                    both of them - Material Design for Bootstrap.
                </p>
                  <hr className='my-5' />

                  <MDBRow id='categories'>
                    <MDBContainer>
                      <DocsLink
                        title='Home'
                        href='https://mdbootstrap.com/docs/react/navigation/sidenav/'
                      />
                    </MDBContainer>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInLeft'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-css.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon
                                icon='css3'
                                brand
                                className='pink-text pr-2'
                              />
                              <strong>CSS</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Animations, colours, shadows, skins and many more!
                              Get to know all our css styles in one place.
                          </MDBCardText>
                            <MDBNavLink
                              tag='button'
                              to='/css'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInDown'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-components.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='cubes' className='blue-text pr-2' />
                              <strong>COMPONENTS</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Ready-to-use components that you can use in your
                              applications. Both basic and extended versions!
                          </MDBCardText>
                            <MDBNavLink
                              tag='button'
                              to='/components'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInRight'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-advanced.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='code' className='green-text pr-2' />
                              <strong>ADVANCED</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Advanced components such as charts, carousels,
                              tooltips and popovers. All in Material Design
                              version.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/advanced'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                  </MDBRow>

                  <MDBRow id='categories'>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInLeft'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-navigation.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='bars' className='pink-text pr-2' />
                              <strong>NAVIGATION</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Ready-to-use navigation layouts, navbars,
                              breadcrumbs and much more! More about our navigation
                              components.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/navigation'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeIn'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-forms.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='edit' className='blue-text pr-2' />
                              <strong>FORMS</strong>
                            </MDBCardTitle>
                            <MDBCardText className='mb-4 pb-3'>
                              Inputselecst, date and time pickers. Everything in
                              one place is ready to use!
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/forms'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInRight'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-tables.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='table' className='green-text pr-2' />
                              <strong>TABLES</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Basic and advanced tables. Responsive, datatables,
                              with sorting, searching and export to csv.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/tables'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                  </MDBRow>

                  <MDBRow id='categories'>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInLeft'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-modals.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon
                                icon='window-restore'
                                far
                                className='pink-text pr-2'
                              />
                              <strong>MODALS</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Modals used to display advanced messages to the
                              user. Cookies, logging in, registration and much
                              more.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/modals'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInUp'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-plugins.png'
                          />
                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon
                                icon='arrows-alt'
                                className='blue-text pr-2'
                              />
                              <strong>PLUGINS & ADDONS</strong>
                            </MDBCardTitle>
                            <MDBCardText>
                              Google Maps, Social Buttons, Pre-built Contact Forms
                              and Steppers. Find out more about our extended
                              components.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/addons'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    {/* PRO-START */}
                    <MDBCol md='4'>
                      <MDBAnimation reveal type='fadeInRight'>
                        <MDBCard cascade className='my-3 grey lighten-4 narrower overlay zoom'>
                          <MDBCardImage
                            cascade
                            className='img-fluid'
                            src='https://web-complett.de/mdb-react-pro/rws/rws-sections.png'
                          />

                          <MDBCardBody cascade className='text-center'>
                            <MDBCardTitle>
                              <MDBIcon icon='th' className='green-text pr-2' />
                              <strong>SECTIONS</strong>
                            </MDBCardTitle>
                            <MDBCardText className='mb-4 pb-3'>
                              E-commerce, contact, blog and much more sections.
                              All ready to use in seconds.
                          </MDBCardText>

                            <MDBNavLink
                              tag='button'
                              to='/sections'
                              target='_parent'
                              color='rws'
                              className='btn btn-outline-rws btn-sm d-inline'
                              onClick={this.scrollToTop}
                            >
                              More
                          </MDBNavLink>
                          </MDBCardBody>
                        </MDBCard>
                      </MDBAnimation>
                    </MDBCol>
                    {/* PRO-END */}
                  </MDBRow>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </div>




        </SectionContainer>

      </Router>
    );
  }
}

export default HomePage;
